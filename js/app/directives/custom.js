angular.module('app.custom.directive', [])

.run(['$rootScope',function ($rootScope) {

   $rootScope.layout = {};
   $rootScope.layout.loading = false;

   $rootScope.$on('$routeChangeStart', function() {

      //show loading gif
      $rootScope.layout.loading = true;

   });

   $rootScope.$on('$routeChangeSuccess', function() {

      //hide loading gif
      $rootScope.layout.loading = false;

   });

   $rootScope.$on('$routeChangeError', function() {
       $rootScope.layout.loading = false;
   });
}])
.directive('loadingAnimation', function(){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            condition: '=',
        },
        template: '<div class="router-animation-loader" ng-hide="!condition.loading"><i class="fa fa-gear fa-4x fa-spin"></i></div>',
    };
})

.directive('onlyNum', function() {
    return function(scope, element, attrs) {

        var keyCode = [8,9,37,39,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105,110,109,189,190];
          element.bind("keydown", function(event) {
            if($.inArray(event.which,keyCode) == -1) {
                scope.$apply(function(){
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
     };
})
.directive('validatedInput', ['$parse', function ($parse) {
    var message = {
        'min':       'Debe ser mayor que ',
        'max':       'Debe ser menor que ',
        'number':    'Debe ser numérico',
        'required':  'Es requerido',
        'maxlength': 'Debe tener menos de ',
        'minlength': 'Debe tener más de '
    };

    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            input: '=',
        },
        template: '<div class="form-group" ng-class="{\'has-error\':hasError, \'has-success\': !hasError }">'+
                  '<label>{{label}}</label>' +
                  '<div ng-transclude></div>' +
                  '<div ng-show="hasError">' +
                    '<span ng-repeat="(key,error) in input.$error" class="help-block" ng-show="input.$error[key] && input.$dirty" ><i class="fa fa-warning"></i> {{input.$error[key] && getMessage(key, input)}}</span>' +
                  '</div>' +
                '</div>',
        link: function (scope, element, attrs, ctrl) {
            scope.label = attrs.label;

            scope.getMessage = function(error, input) {
                var p = '';

                if (error == 'min' || error == 'max') {
                    p = ' '+ element.find('input[name="'+input.$name+'"],textarea[name="'+input.$name+'"],select[name="'+input.$name+'"]').attr(error);
                } else
                if (error == 'minlength' || error == 'maxlength') {
                    p = ' '+ element.find('input[name="'+input.$name+'"],textarea[name="'+input.$name+'"],select[name="'+input.$name+'"]').attr('ng-'+error)+' caracteres';
                }

                return message[error] + p;
            };

            var $inputs = element.find('input[ng-model],textarea[ng-model],select[ng-model]');

            if ($inputs.length > 0) {
                $inputs.each(function() {

                    var $input=$(this);
                    scope.$watch(function() {
                        return $input.hasClass('ng-invalid');
                    }, function(isInvalid) {
                        scope.hasError = isInvalid;
                    });
                });
            }
        }
    };
}])
/**
 * Directiva para upload de archivos usando angular-upload-file y Stapler del lado del servidor
 *
 * <attach-file-field model="model" name="logo" label="Logo" preview="true" onupload="fileattached()" showprogress="true" previewwidth="184"></attach-file-field>
 */
.directive('attachFileField', ['Upload', '$timeout', function (Upload, $timeout) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {
            api: '=?',
            model: '=',
            onupload: '&onupload',
            onerror: '&onerror'
        },
        template: '<div class="form-group">'+
            '<label>{{label}}</label><br>'+
            '<div class="thumbnail text-align-center vcenter" style="margin-bottom:5px;">'+
                '<img ngf-src="file" width="{{previewwidth}}">'+
            '</div>'+
            "<progressbar ng-show=\"showprogress\" class=\"progress-striped active no-margin\" value=\"progress\" type=\"{{type}}\">{{progress_text}}</progressbar>"+
            '<div class="btn-group" style="margin-top:5px;">'+

                '<div ng-show="!hasSelectedFile" class="btn btn-default" type="file" ngf-select="api.selectFile($files)" ngf-multiple="false"  accept="image/*"><i class="fa fa-search"></i> Buscar</div>'+
                '<div ng-show="hasSelectedFile && model.id" class="btn btn-default" ng-click="api.upload()" ng-disabled="uploading"><i class="fa fa-upload" ng-class="{\'fa-spin\':uploading, \'fa-circle-o-notch\': uploading}"></i> Subir</div>'+
                '<div ng-show="hasSelectedFile" class="btn btn-default"  ng-click="api.cancel()" ><i class="glyphicon glyphicon-remove"></i> Cancelar</div>'+
                '<div ng-show="hasFile && !hasSelectedFile" class="btn btn-default"  ng-click="api.delete()" ><i class="fa fa-trash-o"></i> Borrar</div>'+
            '</div>'+
        '</div>',
        link: function ($scope, element, attrs, ctrl) {
            // init scope
            $scope.label           = attrs.label;
            $scope.name            = attrs.name;
            $scope.showprogress    = attrs.showprogress;
            $scope.previewwidth    = attrs.previewwidth||100;
            $scope.hasFile         = false;
            $scope.hasError        = false;
            $scope.hasSelectedFile = false;
            $scope.uploading       = false;
            $scope.file            = false;
            $scope.type            = 'info';

            $scope.api = {};

            // watch model changes
            $scope.$watch('model', function(data) {

                var name = $scope.name + '_file_name';

                if(!$scope.hasSelectedFile) $scope.file = data[$scope.name + '_preview'];

                if(data[name] != null ) {
                    $scope.hasFile = true;
                } else {
                    $scope.hasFile = false;
                }
            }, true);

            $scope.api.cancel = function() {
                $scope.hasSelectedFile = false;
                $scope.file = $scope.model[$scope.name + '_preview'];
            };

            $scope.api.delete = function() {
                if (!$scope.hasFile) return;

                $scope.model.customGET("detachfile", {name: $scope.name}).then(function(){
                    $scope.hasFile = false;
                    $scope.file = {};
                });
            };

            $scope.api.selectFile = function(files) {
                if (files === null|| files.length == 0) return;

                $scope.file = files[0];

                $scope.hasSelectedFile = true;

                $scope.progress = 0;
                $scope.progress_text = '';
            };

            $scope.api.upload = function(callback) {

                if (!$scope.hasSelectedFile) {
                    if (callback) callback(true);
                    return;
                }

                var upurl = globalSmart.apiPrefix+$scope.model.route+'/'+$scope.model.id+'/attachfile';

                $scope.type = 'info';

                var sdata = {};

                sdata[$scope.name] = $scope.file;

                $scope.uploading = true;

                Upload.upload({
                    url: upurl,
                    method: 'POST',
                    withCredentials: true,
                    data: sdata
                }).progress(function(evt) {
                    $scope.progress = 100.0 * evt.loaded / evt.total;
                    $scope.progress_text = $scope.progress+'%';

                }).success(function(data, status, headers, config) {
                    $scope.uploading = false;
                    $scope.hasSelectedFile = false;
                    $scope.hasFile = true;
                    angular.extend($scope.model, data.model);
                    // file is uploaded successfully
                    if ($scope.onupload) $scope.onupload(data);
                    if (callback) callback(true);
                }).error(function(data, status, headers, config) {
                    $scope.uploading = false;
                    $scope.type = 'danger';
                    $scope.api.cancel();
                    $scope.onerror({'data': data});
                    if (callback) callback(false);
                });
            };
        }
    };
}]);