angular.module('app.activity', [])
    .controller('ActivityController', ['$scope', '$http' , function($scope, $http) {
        var ctrl = this,
            items = ctrl.items = $scope.items = [];

        ctrl.loadActiveItemContent = function() {
            angular.forEach(items, function(item) {
                if (item.active) {
                    ctrl.loadItemContent(item);
                }
            });
        };

        ctrl.loadItemContent = function (loadedItem, callback) {
            loadedItem.active = true;

            $http.get(loadedItem.src).then(function(result) {
                var content = result.data;
                if (angular.isDefined(callback)) {
                    callback(content, loadedItem);
                }

                if (angular.isDefined(loadedItem.onLoad)) {
                    loadedItem.onLoad(loadedItem, content);
                }
            });
        };


        ctrl.loadItem = function(loadedItem, callback) {
            angular.forEach(items, function(item) {
                if (item.active && item !== loadedItem) {
                    item.active = false;
                    //item.onDeselect();
                }
            });

            ctrl.loadItemContent(loadedItem, callback);
        };

        ctrl.addItem = function(item) {
            items.push(item);
            if (!angular.isDefined(item.active)) {
                // set the default
                        item.active = false;
            } else if (item.active) {
                if($('#activity').next('.ajax-dropdown').is(':visible')) {
                    ctrl.loadItem(item);
                }
            }
        };

        ctrl.refresh = function(e, loadedItem) {

            //var btn = angular.element(e.currentTarget);
            //btn.button('loading');
            items = [];

            if (angular.isDefined($scope.onRefresh)) {
                $scope.onRefresh($scope, function() {
                    //btn.button('reset');
                }, loadedItem);
            } else {
                //btn.button('reset');
            }
        };
    }])

    .directive('activity', function() {
        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            controller: 'ActivityController',
            scope: {
                onRefresh: '=onrefresh'
            },
            template: '<span data-ng-transclude=""></span>'
        };
    })

    .directive('activityButton', function() {
        return {
            restrict: 'AE',
            require: '^activity',
            replace: true,
            transclude: true,
            controller: function($scope) {

            },
            scope: {
                icon: '@',
                total: '='
            },
            template: '\
                    <span id="activity" class="activity-dropdown">\
                        <i ng-class="icon"></i>\
                        <b class="badge"> {{ total }} </b>\
                    </span>',
            link: function(scope, element, attrs, activityCtrl) {

                attrs.$observe('icon', function(value) {
                    if (!angular.isDefined(value))
                        scope.icon = 'fa fa-user';
                });

                element.on('click', function(e) {
                    var $this = $(this);

                    if ($this.find('.badge').hasClass('bg-color-red')) {
                        $this.find('.badge').removeClassPrefix('bg-color-');
                        $this.find('.badge').text("0");
                        // console.log("Ajax call for activity")
                    }

                    if (!$this.next('.ajax-dropdown').is(':visible')) {
                        $this.next('.ajax-dropdown').fadeIn(150);
                        $this.addClass('active');
                        activityCtrl.loadActiveItemContent();
                    } else {
                        $this.next('.ajax-dropdown').fadeOut(150);
                        $this.removeClass('active');
                    }

                    //var mytest = $this.next('.ajax-dropdown').find('.btn-group > .active > input.active');
                    //console.log(mytest)

                    e.preventDefault();
                });

                if (scope.total > 0) {
                    var $badge = element.find('.badge');
                    $badge.addClass("bg-color-red bounceIn animated");
                }
            }
        };
    })
    .controller('ActivityContentController', ['$scope' , function($scope) {
        var ctrl = this;
        $scope.currentContent = '';

        ctrl.loadContent = function(content, loadedItem) {
            $scope.loadedItem = loadedItem;
            $scope.currentContent = content;
        }
    }])

    .directive('activityContent', ['$compile', '$interval','$rootScope', function($compile, $interval, $rootScope) {
        return {
            restrict: 'AE',
            transclude: true,
            require: '^activity',
            replace: true,
            scope: {
                footer: '=?',
                autoRefresh: '=autorefresh'
            },
            controller: 'ActivityContentController',
            template: '\
                <div class="ajax-dropdown">\
                    <div class="btn-group btn-group-justified" data-toggle="buttons" data-ng-transclude=""></div>\
                    <div class="ajax-notifications custom-scroll">\
                        <div class="alert alert-transparent" ng-hide="currentContent">\
                            <h4>Haz click en un boton para ver los mensajes</h4>\
                            Esta página en blanco ayuda a proteger tu privacidad.\
                        </div>\
                        <i class="fa fa-lock fa-4x fa-border" ng-hide="currentContent"></i>\
                        \
                        <ng-include src="loadedItem.template"/>\
                    </div>\
                    \
                    <span> {{ footer }}\
                        <button type="button" id="activityrefresh" data-loading-text="Cargando..." data-ng-click="refresh($event)" class="btn btn-xs btn-default pull-right" data-activty-refresh-button="">\
                        <i class="fa fa-refresh"></i>\
                        </button>\
                    </span>\
                </div>',
            link: function(scope, element, attrs, activityCtrl) {
                scope.dateFromNow = function(d) {
                    return moment(d).fromNow();
                };
                scope.refresh = function(e) {
                    activityCtrl.refresh(e, scope.loadedItem);
                };

                if (scope.autoRefresh > 0) {
                    var stop = $interval(function() {
                        if($rootScope.currentUser) scope.refresh();
                    }, scope.autoRefresh * 1000);
                }
            }
        };
    }])

    .directive('activityItem', function() {
        return {
            restrict: 'AE',
            require: ['^activity', '^activityContent'],
            scope: {
                src: '=',
                template: '=',
                onLoad: '=onload',
                active: '=?'
            },
            controller: function() {

            },
            transclude: true,
            replace: true,
            template: '\
                <label class="btn btn-default" data-ng-click="loadItem()" ng-class="{active: active}">\
                    <input type="radio" name="activity">\
                    <span data-ng-transclude=""></span>\
                </label>',
            link: function(scope, element, attrs, parentCtrls) {
                var activityCtrl = parentCtrls[0],
                    contentCtrl  = parentCtrls[1];

                //scope.$watch('active', function(active) {
                //
                //    if (active) {
                //        activityCtrl.loadItem(scope, function(content, loadedItem) {
                //
                //            contentCtrl.loadContent(content, loadedItem);
                //        });
                //    }
                //});
                activityCtrl.addItem(scope);

                scope.loadItem = function() {
                    scope.active = true;

                    activityCtrl.loadItem(scope, function(content, loadedItem) {
                        contentCtrl.loadContent(content, loadedItem);
                    });
                };
            }
        };
    })