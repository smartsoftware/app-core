
/* Services */
angular.module('smartApp.base.services', [])
// Sesion de Usuario
    .service('Session', ['$rootScope', function ($rootScope) {
        // si esta logueado vuelvo a setear el usuario
        if (sessionStorage.currentUser != undefined) {
            try {
                $rootScope.currentUser = angular.fromJson(sessionStorage.currentUser);
            } catch (e) {
                $rootScope.currentUser = null;
            }

        } else {
            $rootScope.currentUser = null;
        }

        this.authenticated = function() {
            return $rootScope.currentUser;
        };

        this.create = function (userData) {
            $rootScope.currentUser = userData;
            sessionStorage.currentUser = angular.toJson($rootScope.currentUser);
        };
        this.destroy = function() {
            $rootScope.currentUser = null;
            delete sessionStorage.currentUser;
        };
        return this;
    }])
    // Autenticacion
    .service('Authenticate', [
        '$sanitize',
        'Session',
        '$http',
        function($sanitize, Session, $http)  {
            var authService = {
                /**
                 * Callback para definir la pagina default al loguearse
                 */
                defaultPageCallback: null,

                authenticate: function(email, password) {
                    return $http
                        .post(globalSmart.apiPrefix +'authenticate', {
                            'email': $sanitize(email),
                            'password': $sanitize(password)
                        })
                        .success(function (data) {
                            Session.create(data.user);
                        });
                },
                getDefaultPage: function() {
                    if (this.defaultPageCallback) return this.defaultPageCallback();
                    return '/inicio';
                },
                isAuthenticated: function () {
                    return Session.authenticated();
                },
                check: function(callback) {
                    return $http
                        .get(globalSmart.apiPrefix +'authenticate')
                        .success(function (data) {
                            Session.create(data.user);
                            if(callback) callback(data);
                        });
                },
                isAuthorized: function (authorizedRoles) {
                    if (!angular.isArray(authorizedRoles)) {
                        authorizedRoles = [authorizedRoles];
                    }

                    var user = this.isAuthenticated();

                    if (!user) return false;

                    var roles = _.intersection(user.roles, authorizedRoles);

                    return (roles.length > 0);
                },
                logout: function(callback) {
                     return $http
                        .get(globalSmart.apiPrefix +'authenticate/logout')
                        .then(function(){
                            Session.destroy();
                            if($.isFunction(callback)) callback();
                        });
                }
            };

            return authService;
    }])
    .factory('Flash', ['$rootScope',function($rootScope){
        return {
            show: function(message){
                $rootScope.flash = message;
            },
            clear: function(){
                $rootScope.flash = "";
            }
        };
    }])
    /**
     * Agrega la funcionalidad comun a los controladores de edición
     * simplemente para no repetir código
     */
    .factory('BaseEditCtrlDecorator',['Restangular', function(Restangular){
        return function($scope, restObj, onSave, responseFormat) {
            $scope.model = {};
            $scope.saving = false;
            $scope.errors = false;

            $scope.delete = function() {
                $.SmartMessageBox({
                    title : "<i class=\"fa fa-warning fa-lg text-danger\"></i> Cuidado",
                    content : "Deseas borrar el registro?",
                    buttons : '[No][Si]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "Si") {
                        $scope.model.remove().then(function(resp){
                            if(onSave) onSave();
                        });
                    }
                });
            };

            $scope.save = function() {
                if ($scope.saving) return;
                $scope.saving = true;
                $scope.model.save().then(function(resource) {
                    $scope.saving = false;
                    $scope.errors = false;
                    if(onSave) onSave(resource);
                },function(response) {
                    $scope.saving = false;
                    if (response.data.errors) {
                        $scope.errors = response.data.errors;
                    }
                });
            };

            $scope.resetform = function() {
                $scope.model = Restangular.copy($scope.modelOriginal);
                return false;
            };

            /**
             * Difiero la carga para darle tiempo a que se inicialicen los combos.
             * Esto evita que a veces hagan 2 veces la peticion por los datos iniciales.
             */
            _.delay(function() {
                restObj.get().then(function(model) {
                    if (responseFormat) responseFormat(model);
                    $scope.model = Restangular.copy(model);
                    $scope.modelOriginal = model;
                });
            },100);
        };
    }])
    .factory('BaseCreateCtrlDecorator',['Restangular', function(Restangular){
        return function($scope, Model, onSave) {
            $scope.model = {};
            $scope.saving = false;
            $scope.errors = false;

            $scope.save = function() {

                if ($scope.saving) return;

                $scope.saving = true;

                var p;
                var isNew = true;

                if ($.isFunction($scope.model.save)) {
                    p = $scope.model.save();
                    isNew = false;
                } else {
                    p = Model.post($scope.model);
                }

                p.then(function(resource) {
                    $scope.saving = false;
                    $scope.errors = false;
                    $scope.model = resource;
                    if(onSave) onSave(resource, isNew);
                },function(response) {
                    $scope.saving = false;
                    if (response.data.errors) {
                        $scope.errors = response.data.errors;
                    }
                });
            };
        };
    }])
    .factory('DataTableBaseConfig',['$http', 'settings', function($http, settings){
        return function(DTOptionsBuilder) {
            var op = DTOptionsBuilder.withOption('serverSide', true)
                .withOption('processing', true)
                .withOption('autoWidth', false)
                .withOption('responsive', true)
                .withOption('drawCallback', function(oSettings) {
                    $("[rel=tooltip]").tooltip();
                    // $('.dataTable .dropdown-toggle').dropdown();
                })
                .withOption('sDom', "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withOption('fnServerData', function ( sSource, aoData, fnCallback ) {
                    var o = {};

                    _.map(aoData, function(n) { o[n.name] = n.value; });

                    $http.get(sSource,{
                        params: o
                    }).success(function(data, status, headers, config) {
                        fnCallback(data);
                    });
                })
                .withDataProp('aaData')
                .withPaginationType('simple_numbers');
                // seteo el leguaje (previamentes cargados en settings para no traer mutiples veces el mismo archivo)
                if (settings.currentLang.langCode != 'en') op.withLanguage(settings.datatables.languagues[settings.currentLang.langCode]);
        };
    }])
    ;