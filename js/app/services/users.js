
/* Services */
angular.module('smartApp.users.services', [])
    .factory('Users', ['Restangular', function (Restangular) {
        return Restangular.service('users');
    }])
    .factory('Roles', ['Restangular', function (Restangular) {
        return Restangular.service('roles');
   }])
    ;

