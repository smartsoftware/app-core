/**
 * SmartSoftware
 *
 * Aplicación Angular
 *
 * @author Martín Santangelo (msantangelo@smartsoftware.com.ar)
 */
angular.module('smartApp.config',[])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/login/login.html'
            });
    }])

    .config(['$httpProvider',function($httpProvider){

        var interceptor = function($q, Session) {

            return {

                // optional method
                'responseError': function(rejected) {
                    var onfinish = null;

                    if (rejected.status == 401) {
                        Session.destroy();
                        onfinish = function() {
                            window.location=('#/login');
                        };
                    }

                    // llamo al manejador global de errores en json
                    globalSmart.handleErrorResponse(rejected, onfinish);

                    return $q.reject(rejected);
                }
            };

            // return function(promise){
            //     return promise.then(success, error);
            // };
        };
        $httpProvider.interceptors.push(interceptor);
    }])
    .config(['RestangularProvider',function(RestangularProvider) {
        // ruta base del api
        RestangularProvider.setBaseUrl(globalSmart.apiPrefix);
        // lee los datos de un webservice con paginación
        RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
            var extractedData;
            // .. to look for getList operations
            if (operation === "getList") {
            // .. and handle the data and meta data
                extractedData = data.data;
                if (!extractedData) extractedData = data;
            } else {
                extractedData = data;
            }
            return extractedData;
        });

    }]);