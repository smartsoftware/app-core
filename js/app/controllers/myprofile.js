angular.module('smartApp.myprofile.controllers', [])
    // rutas del modulo
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/myprofile/', {
                templateUrl: 'partials/myprofile',
                controller:  'MyProfileCtrl'
            })
            .when('/myprofile/changepass', {
                templateUrl: 'partials/setpassword',
                controller:  'MyProfilePasswordCtrl'
            })
        ;
    }])
    .controller('MyProfileCtrl', ['$scope', function($scope) {
        $scope.tipo = ($scope.currentUser.roles[0] == 'Propietario')?'Propietario/Inquilino':'Usuario';
    }])
    .controller('MyProfilePasswordCtrl', ['$scope', '$location', '$http',
        function ($scope, $location, $http) {
            $scope.newpass = {};
            $scope.errors = false;

            $scope.save = function() {
                $http({
                    url: globalSmart.apiPrefix+'myprofile/setpassword',
                    method: "POST",
                    data: $scope.newpass
                })
                    .then(function(response) {
                        $scope.errors = false;
                        $location.path('/myprofile');
                    },
                    function(response) { // optional
                        if (response.data.errors) {
                            $scope.errors = response.data.errors;
                        }
                    });
            };
        }
    ]);