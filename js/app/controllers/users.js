angular.module('smartApp.users.controllers', [])
    // rutas del modulo
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/users', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/users/list.html',
                controller:  'UserListCtrl'
            })
            .when('/users/create', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/users/create.html',
                controller:  'UserCreateCtrl'
            })
            .when('/users/changepass/:id', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/users/setpassword.html',
                controller:  'UserPasswordCtrl'
            })
            .when('/users/:id', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/users/edit.html',
                controller:  'UserEditCtrl'
            })
            .when('/users/:id/roles', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/users/roles_list.html',
                controller:  'UserRolesListCtrl'
            })
            .when('/users/:id_user/roles/create', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/roles/edit.html',
                controller:  'UserRolesCreateCtrl'
            })
            .when('/users/:id_user/roles/:id', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/roles/edit.html',
                controller:  'UserRolesEditCtrl'
            })
            .when('/roles', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/roles/list.html',
                controller:  'RolesListCtrl'
            })
            .when('/roles/create', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/roles/create.html',
                controller:  'RolesCreateCtrl'
            })
            .when('/roles/:id', {
                templateUrl: globalSmart.appCoreAssetPath+'app/views/roles/edit.html',
                controller:  'RolesEditCtrl'
            });
    }])

    /**
     * Controladores
     */
    .controller('UserListCtrl', ['$scope', 'DataTableBaseConfig', 'DTOptionsBuilder', 'DTColumnBuilder',
        function ($scope, DataTableBaseConfig, DTOptionsBuilder, DTColumnBuilder) {
            $scope.dtOptions = DTOptionsBuilder.fromSource(globalSmart.apiPrefix +"users/gettable");

            DataTableBaseConfig($scope.dtOptions);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID'),
                DTColumnBuilder.newColumn('image').withTitle('Foto').renderWith(function(d, t, full){
                    return '<img src="'+full.gravatar+'?d=mm&s=50" class="img-thumbnail"/>';
                }),
                DTColumnBuilder.newColumn('nombre').withTitle('Nombre'),
                DTColumnBuilder.newColumn('apellido').withTitle('Apellido'),
                DTColumnBuilder.newColumn('created_at').withTitle('Creado').withClass('desktop'),
                DTColumnBuilder.newColumn('option').withTitle('Options').renderWith(function(data, type, full){
                   return '<a class="btn btn-info btn-xs" href="#/users/'+full.id+'"><i class="fa fa-edit"></i> Editar</a> <a class="btn btn-warning btn-xs" href="#/users/changepass/'+full.id+'"><i class="fa fa-gears"></i> Cambiar Clave</a> <a class="btn btn-success btn-xs" href="#/users/'+full.id+'/roles"><i class="fa fa-gears"></i> Roles</a>';
                })
            ];
        }
    ])
    .controller('UserEditCtrl', ['$scope', 'Users','$routeParams','$location', 'BaseEditCtrlDecorator',
        function ($scope, Users, $routeParams, $location, BaseEditCtrlDecorator) {
             // restangular query
            var restObj = Users.one($routeParams.id);
            // Utilizo el decorador para agregar funcionalidad comun a scope
            BaseEditCtrlDecorator($scope, restObj, function() {
                $location.path('/users');
            });
        }
    ])
    .controller('UserCreateCtrl', ['$scope', 'Users', '$location', 'BaseCreateCtrlDecorator',
        function ($scope, Users, $location, BaseCreateCtrlDecorator) {
            BaseCreateCtrlDecorator($scope, Users, function() {
                $location.path('/users');
            });
        }
    ])
    .controller('UserPasswordCtrl', ['$scope', 'Users','$routeParams','$location', 'Restangular',
        function ($scope, Users, $routeParams, $location, Restangular) {
            $scope.user = {};
            $scope.newpass = {};
            $scope.errors = false;

            $scope.save = function() {
                $scope.user.doPOST($scope.newpass,'setpassword').then(function() {
                    $scope.errors = false;
                    $location.path('/users');
                },function(response) {
                    if (response.data.errors) {
                        $scope.errors = response.data.errors;
                    }
                });
            };

            /**
             * Difiero la carga para darle tiempo a que se inicialicen los combos.
             * Esto evita que a veces hagan 2 veces la peticion por los datos iniciales.
             */
            _.defer(function(){
                Users.one($routeParams.id).get().then(function(user) {
                    $scope.user = Restangular.copy(user);
                    $scope.userOriginal = user;
                });
            },100);
        }
    ])

    /**
     * Roles
     */
    .controller('RolesListCtrl', ['$scope', 'DataTableBaseConfig', 'DTOptionsBuilder', 'DTColumnBuilder',
        function ($scope, DataTableBaseConfig, DTOptionsBuilder, DTColumnBuilder) {
            $scope.dtOptions = DTOptionsBuilder.fromSource(globalSmart.apiPrefix +"roles/gettable");

            DataTableBaseConfig($scope.dtOptions);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID'),
                DTColumnBuilder.newColumn('name').withTitle('Nombre'),
                DTColumnBuilder.newColumn('created_at').withTitle('Creado').withClass('desktop'),
                DTColumnBuilder.newColumn('option').withTitle('Options').renderWith(function(data, type, full){
                   return '<a class="btn btn-info btn-xs" href="#/roles/'+full.id+'"><i class="fa fa-edit"></i> Editar</a>';
                })
            ];
        }
    ])
    .controller('RolesEditCtrl', ['$scope', 'Roles','$routeParams','$location', 'BaseEditCtrlDecorator',
        function ($scope, Roles, $routeParams, $location, BaseEditCtrlDecorator) {
            // restangular query
            var restObj = Roles.one($routeParams.id);
            // Utilizo el decorador para agregar funcionalidad comun a scope
            BaseEditCtrlDecorator($scope, restObj, function() {
                $location.path('/roles');
            });

        }
    ])
    .controller('UserRolesListCtrl', ['$scope', '$routeParams', 'DataTableBaseConfig', 'DTOptionsBuilder', 'DTColumnBuilder',
        function ($scope, $routeParams, DataTableBaseConfig, DTOptionsBuilder, DTColumnBuilder) {
            $scope.dtOptions = DTOptionsBuilder.fromSource(globalSmart.apiPrefix +"users/"+ $routeParams.id +"/roles/gettable");

            DataTableBaseConfig($scope.dtOptions);

            $scope.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID'),
                DTColumnBuilder.newColumn('name').withTitle('Nombre'),
                DTColumnBuilder.newColumn('created_at').withTitle('Creado').withClass('desktop'),
                DTColumnBuilder.newColumn('option').withTitle('Options').renderWith(function(data, type, full){
                   return '<a class="btn btn-info btn-xs" href="#/users/'+$routeParams.id_user+'/roles/'+full.id+'"><i class="fa fa-edit"></i> Editar</a>';
                })
            ];
        }
    ])
    .controller('UserRolesEditCtrl', ['$scope', 'Users','$routeParams','$location', 'BaseEditCtrlDecorator',
        function ($scope, Users, $routeParams, $location, BaseEditCtrlDecorator) {
            // restangular query
            var restObj = Users.one($routeParams.id_user).one('roles',$routeParams.id);
            // Utilizo el decorador para agregar funcionalidad comun a scope
            BaseEditCtrlDecorator($scope, restObj, function() {
                $location.path('/users/'+$routeParams.id_user+'/roles');
            });
        }
    ])
    .controller('RolesCreateCtrl', ['$scope', 'Roles', '$location', 'BaseCreateCtrlDecorator',
        function ($scope, Roles, $location, BaseCreateCtrlDecorator) {
            BaseCreateCtrlDecorator($scope, Roles, function() {
                $location.path('/users');
            });
        }
    ])
    .controller('UserRolesCreateCtrl', ['$scope', '$routeParams', 'Users', 'Roles', '$location', 'BaseCreateCtrlDecorator',
        function ($scope, $routeParams, Users, Roles, $location, BaseCreateCtrlDecorator) {
            var UserRoles = Users.one($routeParams.id_user).all('roles');

            BaseCreateCtrlDecorator($scope, UserRoles, function() {
                $location.path('/users/'+$routeParams.id_user+'/roles');
            });
        }
    ]);
