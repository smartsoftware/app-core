/**
 * SmartSoftware
 *
 * Helpers y configuraciones globales usadas en la aplicación
 */

var globalSmart = {};

/**
 * Configuracion para datatables responsivas
 */
globalSmart.breakpointDefinition = {
    tablet : 1024,
    phone : 480
};

/**
 * Formatea los errores en un msg html
 */
globalSmart.formatError = function(response) {
    var e = response.data;
    var message = '';

    if (!e.errors) return;

    for(var m in e.errors) {
        message += '<b>'+m+':'+'</b><br>';
        for (var i = 0;i < e.errors[m].length; i++) {
            message += ' '+ e.errors[m][i];
        }
    }
    return message;
};


globalSmart.appCoreAssetPath = 'packages/smartsoftware/app-core/';

/**
 * Prefijo utilizado para acceder a los servicios rest con restangular
 * @type {String}
 */
globalSmart.apiPrefix = '/api/v1/';


globalSmart.handleErrorResponse = function(resp, onfinish) {

    var respJson = (resp.data)?resp.data:resp.responseJSON;

    if (resp.status == 403) {
        $.smallBox({
            title : "Prohibido",
            content : 'No tiene permiso para esta operación',
            color : "#C46A69",
            //timeout: 8000,
            icon : "fa fa-warning"
        });
        return;
    }

    if (respJson.flash) {
        $.bigBox({
            title : respJson.flash.title,
            content : respJson.flash.content,
            icon : "fa fa-warning",
            buttons : '[Ok]'
        });

        onfinish();
    }
    if (respJson.error && resp.status != 401) {
        if ($.isPlainObject(respJson.error)) {
            var msg = respJson.error.message;

            if (respJson.error.file) msg += "<br><b>In:</b> "+respJson.error.file;
            if (respJson.error.line) msg += "<br><b>Line:</b> "+respJson.error.line;

            $.smallBox({
                title : "Error",
                content : msg,
                color : "#C46A69",
                //timeout: 8000,
                icon : "fa fa-warning"
            });
        }
        else if (typeof respJson.error == 'string') {
            $.smallBox({
                title : "Error",
                content : respJson.error,
                color : "#C46A69",
                //timeout: 8000,
                icon : "fa fa-warning"
            });
        }
    }
};
