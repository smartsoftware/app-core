<?php


Route::filter('service_auth', function(){
    if(!Auth::check()){
        return Response::json([
            'flash' => [
                'title'   => "<i class='fa fa-warning txt-color-orangeDark'></i> Atención <span class='txt-color-orangeDark'><strong>Usuario no autenticado</strong></span>",
                'content' => 'Ingrese al sistema con su usuario y contraseña'
            ]
        ], 401);
    }
});

Route::filter('user_is_admin', function(){
    if(!Entrust::hasRole('Administrador')){
        App::abort(403);
    }
});