<?php
/**
 * Alpha and spaces
 */
Validator::extend('alpha_spaces', function($attribute, $value)
{
    return preg_match('/^[\p{L} .,\/-]+$/u', $value);
});

Validator::extend('alpha_num_spaces', function($attribute, $value)
{
    return preg_match('/^[\p{L}\p{N} .,\/-]+$/u', $value);
});