<navigation>
    <nav:item data-view="/dashboard" data-icon="fa fa-lg fa-fw fa-home" title="Dashboard" />

@if(Entrust::hasRole('Administrador'))
    <nav:group data-icon="fa fa-lg fa-fw fa-user" title="Usuarios/Permisos">
        <nav:item data-view="/users" data-icon="fa fa-lg fa-fw fa-user" title="Usuarios" />
        <nav:item data-view="/roles" data-icon="fa fa-lg fa-fw fa-group" title="Roles"></nav:item>
        <nav:item data-view="/permissions" data-icon="fa fa-lg fa-fw fa-key" title="Permisos" />
    </nav:group>
@endif
</navigation>
<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>