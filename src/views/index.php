<?php
use Smartsoftware\AppCore\Assets\Timed;
?><!DOCTYPE html>
<html lang="es-ar" data-ng-app="smartApp" id="smartApp">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> SmartSoftware </title>
    <meta name="description" content="Sistema Gestion de Consorcios">
    <meta name="author" content="SmartSoftware SRL ARGENTINA">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support is under construction-->
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> -->

    <!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.-->
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/your_style.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="css/demo.min.css"> -->

    <!-- Angular Related CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="packages/smartsoftware/app-core/css/ng.min.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="packages/smartsoftware/app-core/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="packages/smartsoftware/app-core/img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip
       Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="packages/smartsoftware/app-core/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="packages/smartsoftware/app-core/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="packages/smartsoftware/app-core/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="packages/smartsoftware/app-core/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="packages/smartsoftware/app-core/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="packages/smartsoftware/app-core/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="packages/smartsoftware/app-core/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

  </head>
  <body class="" data-ng-controller="SmartAppController">
    <!-- POSSIBLE CLASSES: minified, fixed-ribbon, fixed-header, fixed-width, top-menu
       You can also add different skin classes such as "smart-skin-0", "smart-skin-1" etc...-->

    <!-- HEADER -->
    <header id="header" data-ng-include="'partials/header'"></header>
    <!-- END HEADER -->

    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->
    <aside id="left-panel">
        <!-- User info -->
        <div class="login-info">
            <span ng-show="currentUser">

                <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                    <img ng-src="{{currentUser.gravatar}}?d=mm&s=28" alt="me" class="online" />
                    <span>
                        {{currentUser.apellido}} {{currentUser.nombre}}
                    </span>
                    <i class="fa fa-angle-down"></i>
                </a>

            </span>
        </div>
        <!-- end user info -->
        <span data-ng-include="currentUser && ('/menu/private?v='+timestamp) || ('/menu/public?v='+timestamp)"></span>
    </aside>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

      <!-- RIBBON -->
      <div id="ribbon" data-ng-include="'partials/ribbon'" ></div>
      <!-- END RIBBON -->

      <!-- LOADING -->
      <loading-animation condition="layout"></loading-animation>
      <!-- MAIN CONTENT -->
      <div id="content" data-ng-view="" class="view-animate"></div>
      <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"><span data-ng-include="'partials/footer'"></span></div>
    <!-- END FOOTER -->

    <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
    Note: These tiles are completely responsive,
    you can add as many as you like
    -->
    <div id="shortcut"><span data-ng-include="'partials/shortcut'"></span></div>
    <!-- END SHORTCUT AREA -->

    <!--================================================== <--></-->
    <!-- combined JS library's (build by GULP)-->
    <script src="<?= Timed::get('librarys.js');?>"></script>

    <?php
    if (!App::isLocal()){
        ?>
        <script src="<?= Timed::get('templates.js');?>"></script>
        <?php
    } else {
        ?>
        <script>
            angular.module("templates", []);
        </script>
        <?php
    }
    ?>

    <!-- MAIN ANGULAR JS FILE (build by GULP)-->
    <script src="<?= Timed::get('app.js');?>"></script>

    <script src="packages/smartsoftware/app-core/js/langs/angular/angular-locale_es-ar.js"></script>

    <script type="text/javascript">
      angular.module("smartApp").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
    </script>
  </body>
</html>