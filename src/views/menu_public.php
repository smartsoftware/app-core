<navigation>
    <nav:item data-view="/dashboard" data-icon="fa fa-lg fa-fw fa-home" title="Dashboard" />
    <nav:item data-view="/login" data-icon="fa fa-lg fa-fw fa-user" title="Ingreso" />
    <nav:item data-view="/users/list" data-icon="fa fa-lg fa-fw fa-user" title="Usuarios" />
</navigation>
<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>