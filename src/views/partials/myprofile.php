<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-8">
                    <div class="well well-light well-sm no-margin no-padding">

                        <div class="row">
                            <div class="col-sm-12" >
                                <div id="myCarousel" class="carousel fade profile-carousel">
                                    <div class="air air-bottom-right padding-10">
                                        <a href="#/myprofile/changepass" class="btn txt-color-white bg-color-orange btn-sm"><i class="fa fa-check"></i> Cambiar Clave</a>
                                    </div>
                                    <div class="carousel-inner">
                                        <!-- Slide 1 -->
                                        <div class="item active">
                                            <img src="packages/smartsoftware/app-core/img/demo/s1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">

                                <div class="row">

                                    <div class="col-sm-3 profile-pic">
                                        <img ng-src="{{currentUser.gravatar}}">
                                        <div class="padding-10">

                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <h1>{{currentUser.apellido}} {{currentUser.nombre}}
                                            <br>
                                            <small> {{tipo}}</small></h1>

                                        <ul class="list-unstyled">
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-phone"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{currentUser.numero_telefono}}</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="">{{currentUser.email}}</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-user"></i>&nbsp;&nbsp; <span class="txt-color-darken">Usuario</span> <span class="txt-color-blue">{{currentUser.username}}</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">Registrado</span> <span class="txt-color-blue">{{currentUser.created_at}}</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">Modificado</span> <span class="txt-color-blue">{{currentUser.update_at}}</span>
                                                </p>
                                            </li>
                                        </ul>
                                        <br>
                                        <p class="font-md">
                                            <i>Imagen</i>
                                        </p>
                                        <p>

                                            Para que se muestre su foto debe tener una cuenta de <a href="https://es.gravatar.com/" target="_blank">gravatar</a> asociada al mismo correo utilizado en el usuario.

                                        </p>

                                        <br>
                                        <br>

                                    </div>

                                </div>

                            </div>

                        </div>


                        <!-- end row -->

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    pageSetUp();
</script>