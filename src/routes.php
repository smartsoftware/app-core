<?php
use Smartsoftware\AppCore\Authorization\Authorize;

$apiprefix = Config::get('app-core::api_prefix');

// Servicios publicos
Route::group(array('prefix' => $apiprefix), function() {
    Route::get('authenticate/confirm/{code}', 'Smartsoftware\AppCore\AuthenticationController@confirm');
    Route::get('authenticate', 'Smartsoftware\AppCore\AuthenticationController@check');
    Route::get('authenticate/logout', 'Smartsoftware\AppCore\AuthenticationController@logout');
    Route::get('authenticate/loginas/{id}', 'Smartsoftware\AppCore\AuthenticationController@loginas');
    Route::post('authenticate', 'Smartsoftware\AppCore\AuthenticationController@login');
});

// servicios que requieren autenticacion de usuario

Authorize::routeNeedsRole($apiprefix.'/users*',['Administrador']);
Authorize::routeNeedsRole($apiprefix.'/roles*',['Administrador']);
Authorize::routeNeedsRole($apiprefix.'/authenticate/loginas*',['Administrador']);

Route::group(
    array(
        'prefix' => Config::get('app-core::api_prefix'),
        'before' => 'service_auth'
    ),
    function() {
        Route::get('myprofile/', 'Smartsoftware\AppCore\MyProfileController@get');
        Route::post('myprofile/setpassword', 'Smartsoftware\AppCore\MyProfileController@setpassword');
    }
);

Route::group(
    array(
        'prefix' => Config::get('app-core::api_prefix')
    ),
    function() {
        //usuarios
        Route::post('users/{id}/setpassword', 'Smartsoftware\AppCore\UsersController@setpassword');
        Route::get('users/gettable', 'Smartsoftware\AppCore\UsersController@gettable');
        Route::resource('users', 'Smartsoftware\AppCore\UsersController');

        //roles
        Route::get('roles/gettable', 'Smartsoftware\AppCore\RolesController@gettable');
        Route::resource('roles', 'Smartsoftware\AppCore\RolesController');

        //usuarios roles
        Route::get('users/{id}/roles/gettable', 'Smartsoftware\AppCore\UsersRolesController@gettable');
        Route::resource('users.roles', 'Smartsoftware\AppCore\UsersRolesController');
    }
);

Route::get('/', function()
{
    return View::make('app-core::index');
});

Route::get('/menu/public', function()
{
    $contents = View::make('app-core::menu_public');
    $response = Response::make($contents, 200);
    $response->headers->set('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    $response->headers->set('Pragma','no-cache');
    $response->headers->set('Expires','Fri, 01 Jan 1990 00:00:00 GMT');

    return $response;
});

Route::get('/menu/private',array('before' => 'service_auth', function()
{
    $contents = View::make('app-core::menu_private');
    $response = Response::make($contents, 200);
    $response->headers->set('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    $response->headers->set('Pragma','no-cache');
    $response->headers->set('Expires','Fri, 01 Jan 1990 00:00:00 GMT');

    return $response;
}));



Route::get('/partials/{view}', function($view)
{
    return View::make('app-core::partials.'.$view);
});
/*
Route::get('/partials/header', function()
{
    return View::make('app-core::header');
});

Route::get('/partials/shortcut', function()
{
    return View::make('app-core::shortcut');
});

Route::get('/partials/ribbon', function()
{
    return View::make('app-core::ribbon');
});

Route::get('/partials/profile', function()
{
    return View::make('app-core::profile');
});
*/
