<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Supuetamente no es una buena practica hacer esto con una migracion
 * pero es lo mas simple dentro de un paquete
 */
class CreatebaseUserroles extends Migration {

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        $id_user = DB::table('users')->insertGetId(
            array(
                'email'      => '',
                'password'   => Hash::make('lagadorcha'),
                'nombre'     => 'Administrador',
                'username'   => 'admin',
                'created_at' => date('Y-m-d H:i:s'),
                'confirmed'  => true
            )
        );

        $id_role = DB::table('roles')->insertGetId(
            array(
                'name'       => 'Administrador',
                'created_at' => date('Y-m-d H:i:s')
            )
        );

        DB::table('assigned_roles')->insert(
            array(
                'user_id'     => $id_user,
                'role_id'     => $id_role,
                // 'created_at' => date('Y-m-d H:i:s')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {

    }

}
