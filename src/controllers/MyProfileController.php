<?php namespace Smartsoftware\AppCore;

use Auth;
use Controller;

class MyProfileController extends Controller
{
    public function get()
    {
        $user = Auth::user();

        $rtn = array_intersect_key($user->toArray(), array_flip(['apellido','nombre','numero_telefono', 'direccion','username','mail']));

        $rtn['roles'] = $user->roles()->select('roles.id','name')->get();

        return $rtn;
    }

    public function setpassword()
    {
        return \App::make('Smartsoftware\AppCore\UsersController')->setpassword(Auth::user()->id);
    }
}