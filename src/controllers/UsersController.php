<?php namespace Smartsoftware\AppCore;

use Smartsoftware\AppCore\PaginatedResource;
use Smartsoftware\AppCore\BaseResourceController;
use Datatable;
use Input;
use Response;

class UsersController extends BaseResourceController {
    /*
     * Paginated
     */
    use PaginatedResource;

    protected $model = 'Smartsoftware\AppCore\Models\User';

    public function gettable()
    {
        return Datatable::query( $this->getQuery() )
                ->showColumns('id','gravatar','email', 'nombre','apellido','created_at')
                ->searchColumns('nombre','apellido')
                ->orderColumns('id','nombre')
                ->setAliasMapping(true)
                ->make();
    }

    protected function validateAndSave($resource, $ids = array())
    {
        $resource->confirmation_code = uniqid();

        // si es nuevo no permito que este confirmado
        if (!$resource->id) {
            $resource->confirmed = 0;
        }

        return parent::validateAndSave($resource, $ids);
    }

    /**
     * Reset User Password
     *
     * @param $id
     * @return \Eloquent
     */
    public function setpassword($id)
    {
        $data = Input::only('password_confirmation', 'password');

        $user = $this->findResource(func_get_args(), __FUNCTION__);

        $user->fill($data);

        if (!$user->isValid('creating')){
             return Response::json(['errors' => $user->getErrors('creating')], 400);
        }

        return $this->validateAndSave($user);
    }
}

