<?php namespace Smartsoftware\AppCore;

use Smartsoftware\AppCore\BaseResourceController;
use Smartsoftware\AppCore\PaginatedResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
use Validator;
use Input;
use Lang;

use Smartsoftware\AppCore\Models\User;

class UsersRolesController extends BaseResourceController {
    use PaginatedResource;

    protected $allowWith = false;

    protected $model = 'Smartsoftware\AppCore\Models\Role';

    protected function getQuery($ids = array(), $with = false)
    {
        return User::findOrFail($ids[0])->roles();
    }

    protected function findResource($ids, $functionName)
    {
        return User::findOrFail($ids[0])->roles()->where('roles.id','=',$ids[1])->firstOrFail();
    }

    public function gettable($id)
    {
        $query = \DB::table('roles')->select('roles.*')
            ->join('assigned_roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->where('assigned_roles.user_id','=',$id);

        return \Datatable::query($query)
                ->showColumns('id','name','created_at')
                ->searchColumns('name')
                ->orderColumns('roles.id','name')
                ->setAliasMapping(true)
                ->make();
    }
}

