<?php namespace Smartsoftware\AppCore;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

use Smartsoftware\AppCore\Models\User;

use Redirect;

class AuthenticationController extends BaseController {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();

        return Response::json([
           'flash' => Lang::get('app-core::authentication.nosession')],
            200
        );
    }

    public function loginas($id)
    {
        $user = User::find($id);

        Auth::login($user);

        return Redirect::to('/');
    }

    public function confirm($code)
    {
        if ( $user = User::where('confirmation_code', '=', $code)->first() )
        {

            if ($user->confirmed == '0') {
                $user->unguard();
                $user->confirmed = 1;
                $user->forceSave();

                Auth::login($user);

                return Redirect::to('/');

            } else {

               return Redirect::to('/');
            }
        }

    }

    public function check()
    {
        if (Auth::check()) {
            return $this->user_response();
        }
    }

    protected function user_response()
    {
        $user = Auth::user()->toArray();
        $user['roles'] = Auth::user()->roles()->get()->lists('name');

        return Response::json([
            'user' => $user],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function login()
    {
        $field = filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $credentials = array(
            $field      => Input::get('email'),
            'password'  => Input::get('password'),
            'confirmed' => 1
        );

        if ( Auth::attempt($credentials) ) {

            return $this->user_response();

        } else {
            return Response::json([
                    'error' => Lang::get('app-core::authentication.wrongcredentials')
                ],
                401
            );
        }
    }
}