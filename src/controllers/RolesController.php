<?php namespace Smartsoftware\AppCore;

use Smartsoftware\AppCore\BaseResourceController;
use Smartsoftware\AppCore\PaginatedResource;

use Datatable;

class RolesController extends BaseResourceController {
    use PaginatedResource;

    protected $allowWith = array('users');

    protected $model = 'Smartsoftware\AppCore\Models\Role';

    public function gettable()
    {
        return Datatable::query( $this->getQuery() )
                ->showColumns('id','name','created_at')
                ->searchColumns('name')
                ->orderColumns('id','name')
                ->setAliasMapping(true)
                ->make();
    }
}

