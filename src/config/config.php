<?php

return array(
    /**
     * URL Prefix for common api
     */
    'api_prefix' => 'api/v1'
);