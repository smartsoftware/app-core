<?php
/**
 * SmartSoftware SRL
 * @author Martin A. Santangelo <msantangelo@smartsoftware.com.ar>
 */
namespace Smartsoftware\AppCore;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Watson\Validating\ValidationException;

use DB;

/**
 * Base class for REST resources controllers for Eloquent and Ardent models
 */
class BaseResourceController extends BaseController {
    /**
     * We limit the quantity of rows a user can retrieve in a single call
     * TO PREVENT D.O.S.
     * @var integer
     */
    protected $maxRows = 100;

    /**
     * Eloquent Model Class
     * @var string
     */
    protected $model;

    /**
     * Display a listing of resources
     *
     * @return Response
     */
    public function index()
    {
        $resources = $this->getList(func_get_args());

        return Response::json($resources, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * List the resources
     *
     * @return Collection Collection of records
     */
    protected function getList($ids)
    {
       return $this->getListQuery($ids)->take($this->maxRows)->get();
    }

    protected function getListQuery($ids)
    {
        return $this->getQuery($ids);
    }

    /**
     * Get the query builder form Eloquent model
     *
     * @param  boolean $with agregar with al query
     * @return Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery($ids = array())
    {
        $m = $this->model;

        return $m::query();
    }

    /**
     * Get resources by id
     * @param  array  $ids           array of ids
     * @param  string $functionName  function name of caller
     * @return query                 [description]
     */
    protected function findResource($ids, $functionName)
    {
        return $this->getQuery($ids)->findOrFail($ids[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $resource = $this->findResource(func_get_args(), __FUNCTION__);
        } catch (ModelNotFoundException $e) {
            return Response::json(['error' => Lang::get('app-core::resources.notfound')], 400);
        }

        return Response::json($resource,200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * get input for update or store
     * @return [array]
     */
    protected function getInputValues($from='')
    {
        return Input::all();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $resource = $this->findResource(func_get_args(), __FUNCTION__);

        $resource->fill($this->getInputValues(__FUNCTION__));

        $resource = $this->validateAndSave($resource);

        return $this->sendResource($resource);
    }

    /**
     * Store a newly created $RESOURCE$ in storage.
     *
     * @return Response
     */
    public function store()
    {
        $m = $this->model;

        $resource = new $m;
        $resource->fill($this->getInputValues(__FUNCTION__));

        $resource = $this->validateAndSave($resource, func_get_args());

        return $this->sendResource($resource);
    }

    protected function sendResource($r)
    {
        if (is_array($r)){
            return Response::json($r,400);
        }
        return Response::json($r, 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * Run validations and save the resource
     *
     * returns 200 response on save or 400 on error
     *
     * @param  \Eloquent $resource Model
     * @param  array    $ids      List of Id's of parent models (for nested controllers)
     * @return \Eloquent          Model
     */
    protected function validateAndSave($resource, $ids = array())
    {
        try {
            DB::beginTransaction();

            //execute before save
            $this->beforeSave($resource);

            if (empty($ids)) {

                if ($resource->save() === false) {
                   return ['errors' => $resource->getErrors(), 'data'=> $resource->toArray()];
                }
            } else {

                if ($this->getQuery($ids)->save($resource) === false) {
                    return ['errors' => $resource->getErrors(), 'data'=> $resource->toArray()];
                }
            }

            //execute after save
            $this->afterSave($resource);

            DB::commit();

        } catch(ValidationException $e) {
            DB::rollback();
            return ['errors' => $e->getErrors()];
        }


        return $resource;
    }

    protected function beforeSave($resource)
    {

    }

    protected function afterSave($resource)
    {

    }

    /**
     * Remove the specified $RESOURCE$ from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $resource = $this->findResource(func_get_args(), __FUNCTION__);
            $resource->delete();
        } catch (ModelNotFoundException $e) {
            return Response::json(['error' => Lang::get('app-core::resources.notfound')], 400);
        }

        return Response::json(['success' => 'true'],200);
    }
}
