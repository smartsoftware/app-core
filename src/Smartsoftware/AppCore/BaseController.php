<?php
/**
 * SmartSoftware SRL
 * @author Martin A. Santangelo <msantangelo@smartsoftware.com.ar>
 */
namespace Smartsoftware\AppCore;

use Controller;
use Event;
use Config;

/**
 * Base class for REST resources controllers for Eloquent and Ardent models
 */
class BaseController extends Controller {

    public function __construct()
    {
        if (Config::get('app.debug')) {
            $this->beforeFilter(function()
            {
                Event::fire('clockwork.controller.start');
            });

            $this->afterFilter(function()
            {
                Event::fire('clockwork.controller.end');
            });
        }
    }
}