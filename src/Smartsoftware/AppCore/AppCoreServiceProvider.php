<?php namespace Smartsoftware\AppCore;

use Illuminate\Support\ServiceProvider;
use Exception;

use Smartsoftware\AppCore\Exception\UserErrorException;
use Smartsoftware\AppCore\Exception\NotAllowedException;
use App;

class AppCoreServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('smartsoftware/app-core');
        include __DIR__.'/../../routes.php';
        include __DIR__.'/../../filters.php';
        include __DIR__.'/../../validators.php';
        include __DIR__.'/../../macros.php';

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Entrust', 'Zizaco\Entrust\EntrustFacade');
        $loader->alias('Datatable', 'Chumper\Datatable\Facades\DatatableFacade');

        $this->app->register('Zizaco\Entrust\EntrustServiceProvider');
        $this->app->register('Chumper\Datatable\DatatableServiceProvider');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        /**
         * Error de usuario (Errores no graves)
         */
        $app->error(function(UserErrorException $exception, $code) use ($app)
        {
            return $app['Response']::json([
                'error' => [
                    'message' => $exception->getMessage(),
                    'code'    => $code
                ]
            ],400);
        });

        /**
         * Excepcion de falta de Permisos
         */
        $app->error(function(NotAllowedException $exception, $code) use ($app)
        {
            return $app['Response']::json([
                'error' => [
                    'message' => $exception->getMessage(),
                    'code'    => 403
                ]
            ],403);
        });

        if (!\Config::get('app.debug')) {
            # Handle Exception errors (Not others)
            $app->error(function(Exception $exception, $code) use ($app)
            {
                /**
                 * Capturo la excepcion de foreing key
                 */
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    if ($exception->getCode() == '23000') {
                        $msg = 'No puedes alterar este registro ya que esta en uso';
                    } else {
                        $msg = 'Error en la Base de Datos';
                    }
                } else {
                    $msg = $exception->getMessage().' '.$exception->getCode();
                }

                if ( strpos($app['request']->header('accept'), 'application/json') !== false )
                {
                    return $app['Response']::json([
                        'error' => [
                            'message' => $msg,
                            'code'    => $code
                        ]
                    ], 500);
                }
            });
        }

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}
