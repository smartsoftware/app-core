<?php namespace Smartsoftware\AppCore\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;
use \Esensi\Model\Model;

class User extends Model implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait, HasRole;

    protected $hashable = [ 'password' ];

    protected $appends = ['gravatar'];

    protected $fillable = array(
        'username',
        'password',
        'apellido',
        'nombre',
        'email',
        'direccion',
        'ciudad',
        'codigo_postal',
        'confirmation_code',
        'confirmed',
        'password_confirmation'
    );

    protected $purgeable = [
        'password_confirmation'
    ];

    protected $rulesets = [
        'creating' => [
            'password' => 'required|alpha_num|between:4,16|confirmed',
            'apellido' => 'required|alpha|min:2',
            'nombre'   => 'required|alpha|min:2',
            'username' => 'required|alpha_num_spaces|between:8,32|unique:users',
            'email'    => 'required|between:3,64|email|unique:users',
            'confirmed'=> 'between:0,1'
        ],
        'updating' => [
            'apellido' => 'required|alpha|min:2',
            'nombre'   => 'required|alpha|min:2',
            'username' => 'required|alpha_num_spaces|between:8,32|unique:users',
            'email'    => 'required|between:3,64|email|unique:users',
            'confirmed'=> 'between:0,1'
        ]
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token', 'confirmation_code');

    public function getGravatarAttribute()
    {
        $hash = md5(strtolower(trim($this->attributes['email'])));
        return "http://www.gravatar.com/avatar/$hash";
    }
}
