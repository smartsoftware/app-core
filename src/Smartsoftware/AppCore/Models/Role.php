<?php namespace Smartsoftware\AppCore\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = array(
        'name'
    );
    public static $rules = array(
        'name' => 'required|between:4,128'
    );

    public function scopeAsignedUser($query, $user_id)
    {
        return $query->whereHas('users', function($q) use ($user_id){
            $q->where('users.id', '=', $user_id);
        });
    }
}