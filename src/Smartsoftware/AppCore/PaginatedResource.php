<?php

/**
 * SmartSoftware SRL
 * @author Martin A. Santangelo <msantangelo@smartsoftware.com.ar>
 */
namespace Smartsoftware\AppCore;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use LaravelBook\Ardent\Ardent;
use Response;
use Input;
use Paginator;

/**
 * Base class for Paginated REST resources controllers for Eloquent and Ardent models
 */
trait PaginatedResource {
    /**
     * Default page size
     * @var integer
     */
    protected $defaultPageSize = 25;

    /**
     * define the input value for current page default 'page' (set in Laravel Paginator)
     * @var string
     */
    protected $pageName = null;

    protected function getPageSize()
    {
        return $this->defaultPageSize;
    }

    protected function getList($ids)
    {
        if ($this->pageName) {
            Paginator::setPageName($this->pageName);
        }
        return $this->getListQuery($ids)->paginate($this->getPageSize());
    }
}