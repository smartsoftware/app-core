<?php namespace Smartsoftware\AppCore\Authorization;

use Auth;
use Response;
use Illuminate\Support\Facades\Facade;

use Entrust;
use Route;

/**
 * Authorization class (using Zizaco\Entrust)
 *
 * @author Martin Santangelo msantangelo@smartsoftware.com.ar
 */
class Authorize
{
    public static $result = null;

    public static function init()
    {
        $result = self::$result;

        $filter_has_role = function ($route, $request) use ($result) {

            $roles = array_slice(func_get_args(),2);

            if(!Auth::check()){
                return Response::json([
                    'flash' => [
                        'title'   => "<i class='fa fa-warning txt-color-orangeDark'></i> Atención <span class='txt-color-orangeDark'><strong>Usuario no autenticado</strong></span>",
                        'content' => 'Ingrese al sistema con su usuario y contraseña'
                    ]
                ], 401);
            }

            $hasARole = array();

            foreach ($roles as $role) {
                if (Entrust::hasRole($role)) {
                    return;
                }
            }

            if(! $result)
                Facade::getFacadeApplication()->abort(403);

            return $result;
        };


        // Same as Route::filter, registers a new filter
        Facade::getFacadeApplication()->router->filter('routeHasRole', $filter_has_role);
    }

    /**
     * Same as Entrust::routeNeedsRole but add:
     *  - suport for http verbs
     *  - return code 401 on user not logued
     *  - cumulative default in false
     */
    public static function routeNeedsRole($route, $roles, $methods = null)
    {
        Facade::getFacadeApplication()->router->when( $route,"routeHasRole:".implode(',', $roles), $methods );
    }

    public static function regexRouteNeedsRole($route, $roles, $methods = null)
    {
        Facade::getFacadeApplication()->router->whenRegex( $route,"routeHasRole:".implode(',', $roles), $methods );
    }

    public static function resourceNeedsRole($resource, $roles, $prefix)
    {
        $uri  = $prefix.Route::getResourceUri($resource);

        $euri = '/'.str_replace('/', '\/', $uri);

        $euri = preg_replace('/\{[a-z]+\}/','[0-9]+',$euri);

        if (!is_array($roles)) {
            $roles = array($roles);
        }

        // verificamos si estan definidos permisos por accion
        $index   = array_key_exists('index', $roles);
        $store   = array_key_exists('store', $roles);
        $update  = array_key_exists('update', $roles);
        $show    = array_key_exists('show', $roles);
        $destroy = array_key_exists('destroy', $roles);

        // tiene definido permisos por acciones
        if ( $index || $store || $show || $update || $destroy ) {
            if ($index) {
                self::regexRouteNeedsRole($euri.'\z/', $roles['index'], ['get']);
            }

            if ($store) {
                self::regexRouteNeedsRole($euri.'\z/', $roles['store'], ['post']);
            }

            if ($update) {
                self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles['update'], ['put', 'patch']);
            }

            if ($show) {
                self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles['show'], ['get']);
            }

            if ($destroy) {
                self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles['destroy'], ['delete']);
            }
        } else {
            // todas las acciones tienen los mismos roles
            self::regexRouteNeedsRole($euri.'\z/', $roles, ['get']);

            self::regexRouteNeedsRole($euri.'\z/', $roles, ['post']);

            self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles, ['put', 'patch']);

            self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles, ['get']);

            self::regexRouteNeedsRole($euri.'\/[0-9]+\z/', $roles, ['delete']);
        }

    }
}