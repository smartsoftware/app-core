<?php
namespace Smartsoftware\AppCore\Traits;


use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Input;

trait SaveRelated
{
    protected function saveDetail($resource, $relation_name, $delete=true)
    {
        // obtengo la relacion
        $relation = $resource->$relation_name();

        if ($relation instanceof HasMany) {
            // input
            $rel_input = $this->getDetailInputValues($relation_name, $resource);

            // borros lo que no estan mas
            if ($delete === true) {
                $this->deleteRelation($relation, $rel_input);
            } else if(is_callable($delete)) {
                $this->customDeleteRelation($relation, $rel_input, $delete);
            }

            foreach($rel_input as $data) {
                if (!is_array($data)) continue;

                $this->createOrUpdateDetail($relation, $data);
            }

        } else if ($relation instanceof HasOne) {

            $data = $this->getDetailInputValues($relation_name, $resource);

            if (!is_array($data)) return;

            $this->createOrUpdateDetail($relation, $data);

        } else if ($relation instanceof BelongsTo) {

            $data = $this->getDetailInputValues($relation_name, $resource);

            if (!is_array($data)) return;

            $this->createOrUpdateDetail($relation, $data);
        }
    }

    protected function getDetailInputValues($relation_name, $resource)
    {
        $input = Input::all();

        return $input[$relation_name];
    }

    protected function createOrUpdateDetail(Relation $relation, $data)
    {
        $model = $relation->getRelated();

        if (array_key_exists('id', $data)) {
            $detail = $model->findOrFail($data['id']);
        } else {
            $detail = $model->newInstance();
        }

        $this->hydrateDetail($detail, $data);

        // guardo segun el tipo de relacion
        if ($relation instanceof HasMany) {

            $relation->save($detail);

        } else if ($relation instanceof HasOne) {

            $relation->save($detail);

        } else if ($relation instanceof BelongsTo) {

            $detail->save();

            $relation->associate($detail);

            $relation->getParent()->save();
        }
    }

    public function hydrateDetail($detail, $data)
    {
        $detail->fill($data);
    }

    /**
     * @param $relation
     * @param $rel_input
     */
    protected function deleteRelation(Relation $relation, $rel_input)
    {
        $destroy_ids = $this->getDeletedIds($relation, $rel_input);

        $relation->getRelated()->destroy($destroy_ids);
    }

    /**
     * @param $relation
     * @param $rel_input
     */
    protected function customDeleteRelation(Relation $relation, $rel_input, $callback)
    {
        $destroy_ids = $this->getDeletedIds($relation, $rel_input);

        $callback($destroy_ids, $relation, $rel_input);
    }

    protected function getDeletedIds(Relation $relation, $rel_input)
    {
        $ids = $relation->lists('id');

        $new_ids = array_fetch($rel_input, 'id');

        $deleted_ids = array_diff($ids, $new_ids);

        return $deleted_ids;
    }
}