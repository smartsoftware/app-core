<?php
/**
 * SmartSoftware SRL
 * @author Martin A. Santangelo <msantangelo@smartsoftware.com.ar>
 */
namespace Smartsoftware\AppCore\Traits;

use Input;
use Response;
use Validator;
/**
 * Agrega soporte de archivos adjuntos usando Stapler
 */
trait AttachedFile {

    /**
     * Attach files to model
     * @return [type] [description]
     */
    public function attachfile()
    {
        $allFiles = Input::file();

        // Traemos el registro
        $resource = $this->findResource(func_get_args(), __FUNCTION__);

        $attachedFiles = $resource->getAttachedFiles();

        $n = 0;

        foreach ($allFiles as $name => $file) {
            // El campo es un campo attachado con stapler y el arhivo subido es valido
            if (array_key_exists($name, $attachedFiles) && $file->isValid()) {
                $n++;
                // set de adjunto al campo

                if (@$resource->rules[$name]) {

                    $validator = Validator::make(
                        array(
                            $name => $file
                        ),
                        array(
                            $name =>  $resource->rules[$name]
                        )
                    );
                    if ($validator->fails()) return Response::json( ['errors' => $validator->messages()], 401);
                }

                $resource->{$name} = $file;
            }
        }

        if ($n > 0) {
            $resource->forceSave();
        }

        return Response::json(['success' => true, 'attached' => $n, 'model' => $resource], 200);
    }

    /**
     * Deatach a file given by 'name' GET parameter
     * @return [type] [description]
     */
    public function detachfile()
    {
        $name = Input::get('name');

        // Traemos el registro
        $resource = $this->findResource(func_get_args(), __FUNCTION__);

        $attachedFiles = $resource->getAttachedFiles();

        // El campo es un campo attachado con stapler
        if ($name && array_key_exists($name, $attachedFiles))
        {
            $resource->{$name} = STAPLER_NULL;
            $resource->forceSave();
        }
    }
}