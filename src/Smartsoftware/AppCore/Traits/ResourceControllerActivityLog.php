<?php

namespace Smartsoftware\AppCore\Traits;

use Input;
use UserActivityLog;
use Response;

trait ResourceControllerActivityLog {
    static $LOG_PAGE_SIZE = 15;
    protected $LOG_EXTRAS_FIELDS = array();

    /**
     * Add extra fields only for this model
     *
     * @param $name
     * @param $value
     */
    public function addLogExtraFields($name, $value)
    {
        $this->LOG_EXTRAS_FIELDS[$name] = $value;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if ($this->getLogInput()) {
            $resource = $this->findResource(func_get_args(), __FUNCTION__);
            return Response::json(
                UserActivityLog::query($this->LOG_EXTRAS_FIELDS)->select('id', 'text','ip_address','moment','user_id')
                    ->with(['user' =>function($q){
                        $q->select('id','nombre','apellido','email');
                    }])
                    ->latest()
                    ->related($resource)
                    ->paginate(self::$LOG_PAGE_SIZE),
                200
            );
        }
        return call_user_func_array('parent::show',func_get_args());
    }

    public function index()
    {
        if ($this->getLogInput()) {
            return Response::json(
                UserActivityLog::query($this->LOG_EXTRAS_FIELDS)->select('id', 'text','ip_address','moment','user_id')
                    ->with(['user' =>function($q){
                        $q->select('id','nombre','apellido','email');
                    }])
                    ->latest()
                    ->related($this->model)
                    ->paginate(self::$LOG_PAGE_SIZE),
                200
            );
        }
        return call_user_func_array('parent::index',func_get_args());
    }

    protected function getLogInput()
    {
        return Input::get('log');
    }
}