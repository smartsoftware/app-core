<?php

return array(

    'nosession'              => 'Sesion expirada',
    'wrongcredentials'       => 'Verifique su usuario y contraseña',
    'wrongcredentials_title' => "<i class='fa fa-warning txt-color-orangeDark'></i> Atención <span class='txt-color-orangeDark'><strong>Autenticacion Fallida</strong></span>"

);