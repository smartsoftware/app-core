<?php

return array(

    'notfound'   => 'Recurso no encontrado',
    'notallowed' => 'No tiene permiso para acceder a este recurso'

);