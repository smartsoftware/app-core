<?php

return array(
    'nosession'        => 'You have been disconnected',
    'wrongcredentials' => 'Wrong user credentials',
    'wrongcredentials_title' => "<i class='fa fa-warning txt-color-orangeDark'></i> Warning <span class='txt-color-orangeDark'><strong>Failed Authentication</strong></span>"
);