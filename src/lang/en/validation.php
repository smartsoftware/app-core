<?php
return array(
    // custom
    "alpha_spaces"     => "The :attribute may only contain letters and spaces.",
    "alpha_num_spaces"  => "The :attribute may only contain letters, numbers and spaces.",
);