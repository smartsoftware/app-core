<?php

return array(

    'notfound'   => 'Resource not found',
    'notallowed' => 'You are not allowed to access this resource'

);