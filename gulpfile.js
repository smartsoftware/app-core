/**
 * Smartsoftware
 */
var gulp          = require('gulp');
var notify        = require('gulp-notify');
var ngcompile     = require('gulp-ngcompile');
var concat        = require('gulp-concat');
var watch         = require('gulp-watch');
var gulpif        = require('gulp-if');
var uglify        = require('gulp-uglify');
var fs            = require('fs');
var argv          = require('yargs').argv;
var ngannotate    = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var minifyHTML    = require('gulp-minify-html');

/**
 * JS librarys
 */
var js_libs = [
    "js/libs/jquery-2.0.2.min.js",
    "js/libs/jquery-ui-1.10.3.min.js",
    "js/libs/lodash.min.js",
    "js/libs/moment-with-langs.js",
    "public/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js",
    "js/libs/bootstrap/bootstrap.min.js",
    "js/libs/notification/SmartNotification.min.js",
    "js/libs/smartwidgets/jarvis.widget.min.js",
    "public/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js",
    "public/js/plugin/sparkline/jquery.sparkline.min.js",
    "public/js/plugin/jquery-validate/jquery.validate.min.js",
    "public/js/plugin/masked-input/jquery.maskedinput.min.js",
    "public/js/plugin/select2/select2.min.js",
    "public/js/plugin/bootstrap-slider/bootstrap-slider.min.js",
    "public/js/plugin/msie-fix/jquery.mb.browser.min.js",
    "public/js/plugin/fastclick/fastclick.min.js",

    //summernote
    "public/js/plugin/summernote/summernote.min.js",

    //flot
    "public/js/plugin/flot/jquery.flot.cust.min.js",
    "public/js/plugin/flot/jquery.flot.resize.min.js",
    "public/js/plugin/flot/jquery.flot.fillbetween.min.js",
    "public/js/plugin/flot/jquery.flot.orderBar.min.js",
    "public/js/plugin/flot/jquery.flot.pie.min.js",
    "public/js/plugin/flot/jquery.flot.tooltip.min.js",

    "js/libs/angular/angular.js",
    "js/app/common.js",
    "js/app/app.config.js",
    "js/libs/app.js",
    "js/libs/speech/voicecommand.min.js",

    "public/js/plugin/datatables/jquery.dataTables.min.js",
    "public/js/plugin/datatables/dataTables.colVis.min.js",
    "public/js/plugin/datatables/dataTables.tableTools.min.js",
    "public/js/plugin/datatables/dataTables.bootstrap.min.js",
    // "public/js/plugin/datatable-responsive/datatables.responsive.min.js",
    "js/libs/dataTables.responsive.min.js"
];

/**
 * Angular App files
 */
var js_angular_app = [
    '../../../public/app/**/*.js',
    './js/app/**/*.js',
    './js/libs/angular/**/*.js'
];

var templates = [
    '../../../public/app/views/**/*.html',
];

fs.exists('../../../gulpinclide.js', function (exists) {
    if(exists) {
        require('../../../gulpinclude.js').include(js_libs, js_angular_app);
    }
});

/**
 * Default Task
 */
gulp.task('build',['build-angular-app','build-js-libs','build-templates']);

gulp.task('default', function(){
     gulp.watch(js_angular_app, ['build-angular-app']);
     //gulp.watch(js_libs, ['build-js-libs']);
     //gulp.watch(templates, ['build-templates']);
});

/**
 * Templates
 */
gulp.task('build-templates', function () {
    gulp.src(templates)
        .pipe(minifyHTML({
          quotes: true
        }))
        .pipe(templateCache({root:'app/views/',standalone: true}))
        .pipe(gulp.dest('../../../public/'))
        .pipe(notify('Angular templates compiled'))
        ;
});

/**
 * Task Construccion de app angular
 */
gulp.task('build-angular-app', function () {
  return gulp.src( js_angular_app )
    .pipe(ngcompile('smartApp')) // app is the module we wish to assemble.
    .pipe(ngannotate())
    .pipe(concat('app.js'))
    .pipe(gulpif(argv.production, uglify()))
    .pipe(gulp.dest('../../../public/'))
    .pipe(notify('Angular App compiled'))
    ;
});

/**
 * Task concatenado de librerias JS
 */
gulp.task('build-js-libs', function () {
  return gulp.src( js_libs )
    .pipe(concat('librarys.js'))
    .pipe(gulpif(argv.production, uglify()))
    .pipe(gulp.dest('../../../public/'))
    .pipe(notify('Js librarys compiled'))
    ;
});