# SmartSoftware app-core
-----------------
Este paquete de Laravel incluye un front-end utilizando el template SmartAdmin y un backend RESTfull en laravel.

Para la validación de los modelos utiliza ensensi/model y Entrust para autorización con usuarios y roles.

## Instalación

Agregar el paquete en composer.json:

    "smartsoftware/app-core"
Hago que lo cargue
    composer update

Agregar config/app.php los providers:

    'Zizaco\Entrust\EntrustServiceProvider',
    'Chumper\Datatable\DatatableServiceProvider',
    'Smartsoftware\AppCore\AppCoreServiceProvider'

Y Facades:

    'Entrust'           => 'Zizaco\Entrust\EntrustFacade',
    'Datatable'         => 'Chumper\Datatable\Facades\DatatableFacade',


Cambiar en app/config/auth.php el modelo a:

    'model' => 'Smartsoftware\AppCore\Models\User',

Crear el archivo app/config/zizaco/entrust/config.php:


```
return array(

    'role' => 'Smartsoftware\AppCore\Models\Role',

    'permission' => 'Smartsoftware\AppCore\Models\Permission'

);
```

Agregar /public/app/app.js

```
   var smartApp = angular.module('smartApp', [
    'ngRoute',
    'restangular',
    'ngSanitize',
    'ui.bootstrap',
    'ui.select',
    'smartApp.config',
    'smartApp.base.controllers',
    'smartApp.users.controllers',
    'smartApp.base.services',
    'smartApp.users.services',
    'app.main',
    'app.navigation',
    'app.localize',
    'app.activity',
    'app.smartui',
    'datatables',

  .....

])
.run(['$rootScope','settings','$http','CSRF_TOKEN', function($rootScope, settings, $http, CSRF_TOKEN) {
    $http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;

    settings.currentLang = settings.languages[0]; // es

    // capturo el evento de cambio de lenguaje y cargo el lenguaje del select2
    $rootScope.$on('localizeLanguageChanged', function(e) {
        if (settings.currentLang.langCode == 'en') return;
        // select2 lang
        loadScript(globalSmart.appCoreAssetPath+"js/langs/select2/select2_locale_"+ settings.currentLang.langCode + ".js");
        // moment lang
        moment.locale(settings.currentLang.langCode);
    });

    // verifico si ya esta iniciada la session
    if (!Authenticate.isAuthenticated()) {
        Authenticate.check();
    }

}])
.factory('settings', ['$rootScope', function($rootScope){
        // supported languages

        var settings = {
            languages: [
                {
                    language: 'English',
                    translation: 'English',
                    langCode: 'en',
                    flagCode: 'us'
                },
                {
                    language: 'Espanish',
                    translation: 'Espanish',
                    langCode: 'es',
                    flagCode: 'es'
                },
                {
                    language: 'German',
                    translation: 'Deutsch',
                    langCode: 'de',
                    flagCode: 'de'
                },
                {
                    language: 'Korean',
                    translation: '한국의',
                    langCode: 'ko',
                    flagCode: 'kr'
                },
                {
                    language: 'French',
                    translation: 'français',
                    langCode: 'fr',
                    flagCode: 'fr'
                },
                {
                    language: 'Portuguese',
                    translation: 'português',
                    langCode: 'pt',
                    flagCode: 'br'
                },
                {
                    language: 'Russian',
                    translation: 'русский',
                    langCode: 'ru',
                    flagCode: 'ru'
                },
                {
                    language: 'Chinese',
                    translation: '中國的',
                    langCode: 'zh',
                    flagCode: 'cn'
                }
            ],

        };

        return settings;

    }]);

```

## Servicios por defecto

### Autenticacion


## Combos
Vista

    <input ui-select2="version4" style="width:100%" ng-model="user.user_id"/>


Controlador Angular

    $scope.version4 = {
        minimumInputLength: 1,
        toAngular: function(data){
            return data.id;
        },
        formatResult: function(data) {
            return '<b>'+data.nombre+'</b> '+data.email;
        },
        formatSelection: function(data){
            return data.nombre;
        },
        initSelection: function(element, callback) {
            var id = $scope.user.user_id;

            if (id) {
                Users.one(id).get().then(function(user) {
                    callback(user);
                });
            }
        },
        query: function (query) {

            Users.getList({'q':query.term}).then(function(users) {
                query.callback({ results: users });
            });
        },
    };

## Autocompletado

    <input autocomplete="off" type="text" ng-model="user.user_id" placeholder="Seleccione el usuario" typeahead="user.id as users.nombre for users in getUsers($viewValue)" typeahead-loading="loadingLocations" class="form-control"  typeahead-input-formatter="formatLabel($model)" typeahead-on-select="selected($item, $model, $label)">
    <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>